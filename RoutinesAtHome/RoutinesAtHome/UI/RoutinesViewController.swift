//
//  RoutinesViewController.swift
//  RoutinesAtHome
//
//  Created by Mario Canto on 7/24/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit
import Commons

public final class RoutinesViewController: UIViewController {
    
    // MARK: Properties
    
    private lazy var tableView: UITableView = {
        let backgroundView = UIView()
        backgroundView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        $0.backgroundView = backgroundView
        $0.separatorStyle = .none
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.dataSource = $1
        $0.delegate = $1
        $0.register(StandardHorizontalScollingCell.self, forCellReuseIdentifier: StandardHorizontalScollingCell.reuseId)
        $0.register(RoutinesHeaderView.self, forHeaderFooterViewReuseIdentifier: RoutinesHeaderView.reuseId)
        return $0
    }(UITableView.init(frame: UIScreen.main.bounds, style: .plain), self)
    
    
    private lazy var searchController: UISearchController = {
        
        $0.obscuresBackgroundDuringPresentation = false
        $0.searchBar.placeholder = "Type something here to search"
        if #available(iOS 11.0, *) {
            $2.navigationItem.searchController = $0
        } else {
            $0.dimsBackgroundDuringPresentation = false
            $0.searchBar.delegate = $2
            $2.definesPresentationContext = true
            $1.tableHeaderView = $0.searchBar
            $0.searchBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            $0.searchBar.barStyle = .black
            UISearchBar().tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            $0.searchBar.setSearchFieldBackgroundImage(UIImage(named: "bg_search", in: Bundle.init(for: RoutinesViewController.self), compatibleWith: nil),
                                                       for: .normal)
        }
        return $0
    }(UISearchController(searchResultsController: nil), tableView, self)
    
    // MARK: Initializers
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: Common Setup
    
    private func setupSubviews() {
        view.addSubview(tableView)
        addContraints(to: tableView)
        
    }
    
    private func addContraints(to tableView: UITableView) {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
            ])
    }
    
    private func skinUI() {
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        let attrs: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.medium)
            ]
        navigationController?.navigationBar.titleTextAttributes = attrs
    }
    
}

// MARK: UIViewController life cycle

extension RoutinesViewController {
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupSearch()
        if #available(iOS 11.0, *) {
        } else {
            extendedLayoutIncludesOpaqueBars = true
            edgesForExtendedLayout = []
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        skinUI()
        navigationItem.title = NSLocalizedString("Rutinas en casa", comment: "")
    }
}

// MARK: Search

extension RoutinesViewController {
    private func setupSearch() {
        searchController.searchResultsUpdater = self
    }
}

extension RoutinesViewController: UISearchBarDelegate {
    
}

extension RoutinesViewController: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    
}


// MARK: RoutinesViewController - Factory Methods

extension RoutinesViewController {
    public static func makeRoutinesViewController() -> RoutinesViewController {
        let viewController = RoutinesViewController(nibName: nil, bundle: nil)
        return viewController
    }
}

// MARK: RoutinesViewController - UITableViewDataSource

extension RoutinesViewController: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StandardHorizontalScollingCell = tableView.dequeueReusableCell(at: indexPath)
        cell.enableLargeCells(indexPath.section == 0)
        cell.setItems()
        return cell
    }
}

extension RoutinesViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(ofType: RoutinesHeaderView.self, at: section)
        headerView.titleLabel.text = "Latest seen"
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UIScreen.main.bounds.height * 0.30
        } else {
            return UIScreen.main.bounds.height * 0.20
        }
    }
}


protocol HorizontalItemsPresentable: AnyObject {
    var collectionView: UICollectionView { get }
    var collectionViewFlowLayout: UICollectionViewFlowLayout { get }
    var enableLargeCells: Bool { set get }
    func enableLargeCells(_ enableLargeCells: Bool)
    func setItems()
}

extension HorizontalItemsPresentable {
    func setItems() {
        collectionView.reloadData()
    }
    
    func enableLargeCells(_ enableLargeCells: Bool) {
        self.enableLargeCells = enableLargeCells
        /*
        let heightMultiplier: CGFloat = enableLargeCells ? UIScreen.main.bounds.height * 0.30 - (4 * 2) : UIScreen.main.bounds.height * 0.20 - (4 * 2)
        let widthMultiplier: CGFloat = enableLargeCells ? 0.92 : 0.44
        let itemSize = CGSize(width: widthMultiplier * UIScreen.main.bounds.width,
                                                   height: heightMultiplier - (collectionViewFlowLayout.sectionInset.top + collectionViewFlowLayout.sectionInset.bottom) - (collectionView.contentInset.top + collectionView.contentInset.bottom))
//        collectionViewFlowLayout.itemSize = itemSize
//        collectionView.invalidateIntrinsicContentSize()
        */
    }
}

class StandardHorizontalScollingCell: UITableViewCell, HorizontalItemsPresentable {
    
    // MARK: Properties
    
    var enableLargeCells: Bool = false
    internal let itemSpacing: CGFloat = 4
    
    internal lazy var collectionViewFlowLayout: UICollectionViewFlowLayout = {
        
        $0.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        $0.scrollDirection = .horizontal
        return $0
    }(UICollectionViewFlowLayout())
    
    internal lazy var collectionView: UICollectionView = {
        $0.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.dataSource = $1
        $0.delegate = $1
        $0.register(RoutineCollectionViewCell.self, forCellWithReuseIdentifier: RoutineCollectionViewCell.reuseId)
        return $0
    }(UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: collectionViewFlowLayout), self)
    
    // MARK: Initializers
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Common Setup
    
    private func setupSubviews() {
        contentView.addSubview(collectionView)
        addContraints(to: collectionView)
    }
    
    private func addContraints(to collectionView: UICollectionView) {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0)
            ])
        collectionView.reloadData()
    }
}

// MARK: HorizontalScollingCell - UICollectionViewDataSource

extension StandardHorizontalScollingCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RoutineCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
        cell.setImage(UIImage(named: "dog",
                              in: Bundle(for: RoutineCollectionViewCell.self), compatibleWith: nil))
        cell.setTitle("Inbike")
        return cell
    }
}

extension StandardHorizontalScollingCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let heightMultiplier: CGFloat = enableLargeCells ? UIScreen.main.bounds.height * 0.30 - (4 * 2) : UIScreen.main.bounds.height * 0.20 - (4 * 2)
        let widthMultiplier: CGFloat = enableLargeCells ? 0.92 : 0.44
        let itemSize = CGSize(width: widthMultiplier * UIScreen.main.bounds.width,
                              height: heightMultiplier - (collectionViewFlowLayout.sectionInset.top + collectionViewFlowLayout.sectionInset.bottom) - (collectionView.contentInset.top + collectionView.contentInset.bottom))
        
        return itemSize
    }
}

extension StandardHorizontalScollingCell: UICollectionViewDelegate {}




// MARK: RoutinePresentable

protocol RoutinePresentable: AnyObject {
    var imageView: UIImageView { get }
    var textLabel: UILabel { get }
    
    func setImage(_ image: UIImage?)
    func setTitle(_ title: String)
}

extension RoutinePresentable {
    
    func setImage(_ image: UIImage?) {
        imageView.image = image
    }
    
    func setTitle(_ title: String) {
        textLabel.text = title
    }
}


final class RoutineCollectionViewCell: UICollectionViewCell, RoutinePresentable {
    
    // MARK: Properties
    
    internal lazy var imageView: UIImageView = {
        $0.layer.cornerRadius = 6
        $0.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        $0.setContentHuggingPriority(.defaultLow, for: .vertical)
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIImageView.init(frame: CGRect.zero))
    
    internal lazy var textLabel: UILabel = {
        $0.textAlignment = .center
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.numberOfLines = 0
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel(frame: CGRect.zero))
    
    private lazy var stackView: UIStackView = {
        $0.spacing = 4
        $0.axis = .vertical
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [imageView, textLabel]))
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Common Setup
    
    private func setupSubviews() {
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0)
            ])
    }
    
   
}

final class RoutinesHeaderView: UITableViewHeaderFooterView {
    
    internal lazy var titleLabel: UILabel = {
        $0.textAlignment = .left
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.numberOfLines = 0
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel(frame: CGRect.zero))
    
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        textLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupSubviews() {
        contentView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0)
            ])
    }
}
