//
//  EventsViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/27/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class EventsViewController: UIViewController {
    
    enum EventType: Int {
        case upcoming
        case past
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.contentInset = UIEdgeInsets(top: 70, left: 0, bottom: 0, right: 0)
        }
    }
    
    private var eventType: EventType  = .upcoming {
        didSet {
            tableView.reloadSections([0], with: .automatic)
        }
    }
    
    
    @objc private func segmentedControlDidChange(_ sender: SegmentedControl) {
        guard let type = EventType(rawValue: sender.selectedSegmentedIndex) else {
            return
        }
        eventType = type
    }
}

extension EventsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let segmentedControl = SegmentedControl(
            items: [NSLocalizedString("Próximos", comment: ""),
                    NSLocalizedString("Pasados", comment: "")])
        segmentedControl.selectedSegmentedIndex = 0
        segmentedControl.addTarget(self,
                                   action: #selector(self.segmentedControlDidChange(_:)),
                                   for: .valueChanged)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.tintColor = #colorLiteral(red: 0.9058823529, green: 0.1450980392, blue: 0.1882352941, alpha: 1)
        view.addSubview(segmentedControl)
        
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            segmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 32),
            segmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32),
            segmentedControl.heightAnchor.constraint(equalToConstant: 34)
            ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let indexPathForSelectedRow = tableView.indexPathForSelectedRow else {
            return
        }
        tableView.deselectRow(at: indexPathForSelectedRow, animated: animated)
    }
}

extension EventsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch eventType {
        case .upcoming:
            let cell: UpcomingEventCell = tableView.dequeueReusableCell(at: indexPath)
            cell.selectionStyle = .none
            return cell
        case .past:
            let cell: PastEventCell = tableView.dequeueReusableCell(at: indexPath)
            cell.selectionStyle = .none
            return cell
        }
    }
    
}



final class UpcomingEventCell: UITableViewCell, EventDescriptionPresentable, EventDatePresentable {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
}

final class PastEventCell: UITableViewCell, EventDescriptionPresentable, EventRatingPresentable, EventSubscriptionsPresentable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet var ratingStars: [UIImageView] = []
    @IBOutlet weak var subscriptionsLabel: UILabel!
    
}






