//
//  PastEventDetailViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/26/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class PastEventDetailViewController: UIViewController {
        
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var pageControl: UIPageControl!
    
    private var pageViewController: UIPageViewController?
    private var selectedPageIndex: Int = 0 {
        didSet {
            pageControl.currentPage = selectedPageIndex
        }
    }
    
    private lazy var viewControllers: [ImagePageViewController] = {
        return (0...5).map({ _ in ImagePageViewController.makeImagePageViewController() })
    }()
    
    @objc @IBAction private func backButtonWasPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
}


extension PastEventDetailViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let pageViewController as UIPageViewController:
            pageViewController.dataSource = self
            pageViewController.delegate = self
            self.pageViewController = pageViewController
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers.enumerated().forEach({
            if $0.offset % 2 == 0 {
                $0.element.imageView.image = UIImage(named: "dog")
            }
        })
        pageControl.numberOfPages = viewControllers.count
        restartAction(sender: self)
    }
}

extension PastEventDetailViewController {
   
}

extension PastEventDetailViewController {
    static func makePastEventDetailViewController() -> PastEventDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: PastEventDetailViewController.reuseId) as! PastEventDetailViewController
    }
}

extension PastEventDetailViewController {
    private func restartAction(sender: AnyObject) {
        pageViewController?.setViewControllers([contentViewControllerAtIndex(selectedPageIndex)], direction: .forward, animated: true, completion: nil)
    }
    
    private func contentViewControllerAtIndex(_ index: Int) -> UIViewController {
        return viewControllers[index]
    }
}

extension PastEventDetailViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                                   viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = (viewControllers as [UIViewController]).index(of: viewController), index + 1 < viewControllers.count else { return nil }
        return contentViewControllerAtIndex((index + 1) % viewControllers.count)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                                   viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = (viewControllers as [UIViewController]).index(of: viewController), index - 1 >= 0 else { return nil }
        return contentViewControllerAtIndex((index - 1) % viewControllers.count)
    }
}

extension PastEventDetailViewController: UIPageViewControllerDelegate {
    public func pageViewController(_ pageViewController: UIPageViewController,
                                   didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if finished {
            guard let currentViewController = pageViewController.viewControllers?.last else { return }
            guard let index = (viewControllers as [UIViewController]).index(of: currentViewController) else { return }
            selectedPageIndex = index
        }
    }
}

