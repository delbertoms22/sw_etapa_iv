//
//  DesignableButton.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/31/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableButton: UIButton {}
