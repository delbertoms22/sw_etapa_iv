//
//  UpcomingEventContainerView.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/27/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit



@IBDesignable
final class EventContainerView: UIView {
    
    enum ContainerType: Int {
        case upcoming
        case past
    }
    
    var _containerType: ContainerType = .upcoming
    @IBInspectable
    var containerType: Int {
        set {
            guard let type = ContainerType(rawValue: newValue) else {
                return
            }
            _containerType = type
        }
        get {
            return _containerType.rawValue
        }
    }
    
    
//    private let cornerRadius: CGFloat = 6
    
    override open var bounds: CGRect {
        didSet(oldBounds) {
            if oldBounds.height != bounds.height {
//                layer.cornerRadius = cornerRadius
                invalidateIntrinsicContentSize()
            }
        }
    }
    
    override open var frame: CGRect {
        didSet(oldBounds) {
            if oldBounds.height != frame.height {
//                layer.cornerRadius = cornerRadius
                invalidateIntrinsicContentSize()
            }
        }
    }
    
    override var intrinsicContentSize: CGSize {
        switch _containerType {
        case .upcoming:
            return CGSize(width: UIScreen.main.bounds.width - 16 * 2, height: UIScreen.main.bounds.height / 9.0)
        case .past:
            return CGSize(width: UIScreen.main.bounds.width - 16 * 2, height: UIScreen.main.bounds.height / 3.0)
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        updateCornerRadius()
    }
    
    private func updateCornerRadius() {
        clipsToBounds = true
//        layer.cornerRadius = cornerRadius
    }
}
