//
//  DashedTextField.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/31/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

@IBDesignable
class DashedTextField: UITextField {
    
    override open var bounds: CGRect {
        didSet(oldBounds) {
            if oldBounds.height != bounds.height {
                setNeedsDisplay()
                invalidateIntrinsicContentSize()
            }
        }
    }
    
    override open var frame: CGRect {
        didSet(oldBounds) {
            if oldBounds.height != frame.height {
                setNeedsDisplay()
                invalidateIntrinsicContentSize()
            }
        }
    }
    
//    override var intrinsicContentSize: CGSize {
//        return CGSize(width: bounds.width, height: 34)
//    }
    
    private lazy var shapeLayer: CAShapeLayer = {
        $0.backgroundColor = UIColor.clear.cgColor
        $1.layer.addSublayer($0)
        return $0
    }(CAShapeLayer(), self)
    
    override func draw(_ rect: CGRect) {
        drawLineInRect(rect)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 8, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 8, dy: 0)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 8, dy: 0)
    }
    
    private func drawLineInRect(_ rect: CGRect) {
        
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        
        
        path.close()
        
        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = 1
        shapeLayer.strokeColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        shapeLayer.fillColor = UIColor.clear.cgColor
        addLineDashPatternToShapeLayer(shapeLayer)
    }
    
    private func addLineDashPatternToShapeLayer(_ shapeLayer: CAShapeLayer) {
        let pattern: [NSNumber] = [4.0, 3.0]
        shapeLayer.lineDashPattern = pattern
    }

}
