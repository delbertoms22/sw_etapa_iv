//
//  ShareManager.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 8/13/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class ShareManager: NSObject {
    
    static let shared = ShareManager()
    
    private override init() {
        super.init()
    }
    
    
    func shareURL(_ url: String, from viewController: UIViewController, animated: Bool = true) {
        let items = [url]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        viewController.present(activityViewController, animated: animated)
    }
}

extension ShareManager: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType?) -> Any? {
        return ""
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String {
        guard let activityType = activityType else {
            return ""
        }
        
        if activityType == .postToTwitter {
            return ""
        }
        
        if activityType == .postToFacebook {
            return ""
        }
        
        
        
        
        return ""
    }
    
    
}

extension ShareManager {
    func share(in viewController: UIViewController, title: String, image: UIImage?, items: [Any], action: @escaping (([Any]) -> Void)) {
        let customItem = CustomActivity(title: title, image: image, performAction: action)
        
        
        let ac = UIActivityViewController(activityItems: items, applicationActivities: [customItem])
        ac.excludedActivityTypes = [.postToFacebook]
        viewController.present(ac, animated: true)
    }
}

final class CustomActivity: UIActivity {
    var _activityTitle: String
    var _activityImage: UIImage?
    var activityItems = [Any]()
    var action: ([Any]) -> Void
    
    init(title: String, image: UIImage?, performAction: @escaping ([Any]) -> Void) {
        _activityTitle = title
        _activityImage = image
        action = performAction
        super.init()
    }
    
    override var activityTitle: String? {
        return _activityTitle
    }
    
    override var activityImage: UIImage? {
        return _activityImage
    }
    
    override var activityType: UIActivityType? {
        return UIActivityType(rawValue: "com.sportsworld.share")
    }
    
    override class var activityCategory: UIActivityCategory {
        return .action
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        return true
    }
    
    override func prepare(withActivityItems activityItems: [Any]) {
        self.activityItems = activityItems
    }
    
    override func perform() {
        action(activityItems)
        activityDidFinish(true)
    }
}
