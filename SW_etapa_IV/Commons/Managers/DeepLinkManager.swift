//
//  DeepLinkManager.swift
//  Deeplinks Test
//
//  Created by Mario Canto on 8/7/18.
//  Copyright © 2018 The Great Pumpkin. All rights reserved.
//

import Foundation
import UIKit

protocol DeepLink {
    func trigger()
}

struct RoutineDetailDeepLink: DeepLink {
    
    private let routineID: String
    
    init(info: [String: Any]) {
        self.routineID = info["id"] as! String
    }
    
    func trigger() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: PaymentsViewController = storyboard.makeViewController()
        vc.loadViewIfNeeded()
        if Thread.isMainThread {
            UIApplication
                .shared
                .keyWindow?
                .rootViewController?
                .present(vc, animated: true, completion: nil)
        } else {
            DispatchQueue.main.async {
                
                UIApplication
                    .shared
                    .keyWindow?
                    .rootViewController?
                    .present(vc, animated: true, completion: nil)
            }
        }
    }
}

final class DeepLinkManager {
    
    enum LinkType: String, CustomStringConvertible {
        case detail
        case none
        
        var description: String {
            switch self {
            case .detail:
                return NSLocalizedString("Details", comment: "")
            case .none:
                return NSLocalizedString("None", comment: "")
            }
        }
    }
    
    struct DeepLinkDescriptor<A> {
        let type: DeepLinkManager.LinkType
        let convert: ([String: Any]) -> A
        
        public init(type: DeepLinkManager.LinkType, convert: @escaping ([String: Any]) -> A) {
            self.type = type
            self.convert = convert
        }
    }
    
    static let shared = DeepLinkManager()
    
    init() {}

}

func parseURL(_ url: URL) -> [String] {
    let urlString = url.absoluteString
    return urlString.components(separatedBy: "/")
}

extension DeepLinkManager {
    
    typealias OpenURLOptions = [UIApplicationOpenURLOptionsKey : Any]
    
    @discardableResult
    func application(_ app: UIApplication, open url: URL, options: OpenURLOptions = [:]) -> Bool {
        
        guard url.host != nil else {
            return true
        }
        
        let components = parseURL(url)
        
        let userInfo = ["id": components.last!]
        self.application(application: app, didReceiveDeepLinkRequest: userInfo)
        
        return true
    }
}

extension DeepLinkManager {
    
    
    
    private func application(application: UIApplication, didReceiveDeepLinkRequest userInfo: [String : Any]) {
        if application.applicationState == UIApplicationState.background || application.applicationState == UIApplicationState.inactive {
            // Save for later use
            let descriptor = DeepLinkDescriptor<RoutineDetailDeepLink>(type: .detail, convert: RoutineDetailDeepLink.init)
            let link = descriptor.convert(userInfo)
            link.trigger()
        } else {
            let descriptor = DeepLinkDescriptor<RoutineDetailDeepLink>(type: .detail, convert: RoutineDetailDeepLink.init)
            let link = descriptor.convert(userInfo)
            link.trigger()
        }
    }
}
