//
//  Button.swift
//  TouchKit
//
//  Created by Mario Canto on 3/27/18.
//  Copyright © 2018 Mario Canto. All rights reserved.
//

import UIKit

@IBDesignable
open class Button: UIButton {
	
//    private let cornerRadius: CGFloat = 6
    
	public var defaultHeight: CGFloat = 54.0 {
		didSet {
			
		}
	}
	
	override open var bounds: CGRect {
		didSet(oldBounds) {
			if oldBounds.height != bounds.height {
//                layer.cornerRadius = cornerRadius
				invalidateIntrinsicContentSize()
			}
		}
	}
	
	override open var frame: CGRect {
		didSet(oldBounds) {
			if oldBounds.height != frame.height {
//                layer.cornerRadius = cornerRadius
				invalidateIntrinsicContentSize()
			}
		}
	}
	
	override open var intrinsicContentSize: CGSize {
		let textSize = titleLabel?.intrinsicContentSize ?? CGSize.zero
		return CGSize(width: textSize.width + titleEdgeInsets.left + titleEdgeInsets.right + bounds.height,
					  height: max(textSize.height, defaultHeight) + titleEdgeInsets.top + titleEdgeInsets.bottom)
	}
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        updateCornerRadius()
    }
    
    private func updateCornerRadius() {
        clipsToBounds = true
//        layer.cornerRadius = cornerRadius
    }
}
