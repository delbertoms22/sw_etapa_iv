//
//  SegmentedControl.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/27/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

@IBDesignable
final class SegmentedControl: UIControl {
    
    @IBInspectable
    var selectedSegmentedIndex: Int {
        set {
            segmentedControl.selectedSegmentIndex = newValue
        }
        get {
            return segmentedControl.selectedSegmentIndex
        }
    }
    
    var _items: [String] = []
    @IBInspectable
    var items: [String] {
        set {
            _items = newValue
            segmentedControl.removeAllSegments()
            _items.enumerated().forEach({ [weak self] in
                self?.segmentedControl.insertSegment(withTitle: $0.element, at: $0.offset, animated: false)
            })
            setNeedsLayout()
        }
        get {
            return _items
        }
    }
    
    let segmentedControl: UISegmentedControl
    
    init(items: [Any]) {
        self.segmentedControl = {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            $0.selectedSegmentIndex = 0
            return $0
        }(UISegmentedControl(items: items))
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 34))
        self.segmentedControl.addTarget(self, action: #selector(self.segmentedControlDidChangeValue(_:)), for: .valueChanged)
        addSubview(segmentedControl)
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            segmentedControl.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            segmentedControl.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            segmentedControl.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            ])
        self.clipsToBounds = true
    }
    
    
    override init(frame: CGRect) {
        self.segmentedControl = {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            $0.selectedSegmentIndex = 0
            return $0
        }(UISegmentedControl(items: []))
        super.init(frame: frame)
        self.segmentedControl.addTarget(self, action: #selector(self.segmentedControlDidChangeValue(_:)), for: .valueChanged)
        addSubview(segmentedControl)
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            segmentedControl.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            segmentedControl.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            segmentedControl.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            ])
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.segmentedControl = {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            $0.selectedSegmentIndex = 0
            return $0
        }(UISegmentedControl(items: []))
        
        super.init(coder: aDecoder)
        self.segmentedControl.addTarget(self, action: #selector(self.segmentedControlDidChangeValue(_:)), for: .valueChanged)
        addSubview(segmentedControl)
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            segmentedControl.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            segmentedControl.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            segmentedControl.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            ])
        self.clipsToBounds = true
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        items = ["PayPal", "Tarjeta"]
        segmentedControl.selectedSegmentIndex = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        layer.borderColor = tintColor.cgColor
        layer.borderWidth = 1
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 34)
    }
    
    @objc private func segmentedControlDidChangeValue(_ sender: UISegmentedControl) {
        sendActions(for: .valueChanged)
    }
    
}
