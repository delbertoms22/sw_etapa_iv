//
//  UIViewController+Toolbars.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 8/1/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

private var keyboardHandlerKey: Void?
private var associatedTextFieldKey: Void?

extension UIViewController {
    
    var keyboardHandler: ( (String) -> Void )? {
        get {
            return objc_getAssociatedObject(self, &keyboardHandlerKey) as? ( (String) -> Void )
        }
        
        set {
            objc_setAssociatedObject(self,
                                     &keyboardHandlerKey, newValue,
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var associatedTextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &associatedTextFieldKey) as? UITextField
        }
        
        set {
            objc_setAssociatedObject(self,
                                     &associatedTextFieldKey, newValue,
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    
    func addToolBarInTextField(_ textField: UITextField) {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = #colorLiteral(red: 0.9019607843, green: 0.1607843137, blue: 0.2156862745, alpha: 1)
        toolBar.barTintColor = #colorLiteral(red: 0.9521105886, green: 0.956189096, blue: 0.9692507386, alpha: 1)
        
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: UIBarButtonItemStyle.done, target: self, action: #selector(UIViewController.donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        //        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    
    @objc func donePressed(){
        view.endEditing(true)
    }
    @objc func cancelPressed(){
        view.endEditing(true) // or do something
    }
}
