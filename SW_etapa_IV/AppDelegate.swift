//
//  AppDelegate.swift
//  SW_etapa_IV
//
//  Created by Aldo Gutierrez Montoya on 7/24/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    typealias LaunchOptions = [UIApplicationLaunchOptionsKey: Any]
    lazy var window: UIWindow? = {
        return $0
    }(UIWindow(frame: UIScreen.main.bounds))


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: LaunchOptions?) -> Bool {
        
        
        let navigationController = UINavigationController()
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.backgroundColor = .clear
        navigationController.navigationBar.isTranslucent = true
        let attrs: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.medium)
        ]
        navigationController.navigationBar.titleTextAttributes = attrs
        navigationController.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let eventsViewController: RoutinesViewController = storyboard.makeViewController()
        let eventsViewController: PaymentsViewController = storyboard.makeViewController()
        navigationController.viewControllers = [eventsViewController]
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return DeepLinkManager.shared.application(app, open: url)
    }
}

