//
//  PaymentsViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 8/1/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class PaymentsViewController: UIViewController {

    private let maxNumberOfCharacters = 16
    private let maxMonth = 2
    private let maxYear = 2
    private let maxCVV = 3
    
    private lazy var yearPickerDataSource: YearPickerDataSourceAndDelegate = {
        return $0
    }(YearPickerDataSourceAndDelegate())
    
    private lazy var yearPickerView: UIPickerView = {
        $0.showsSelectionIndicator = true
        $0.dataSource = $1
        $0.delegate = $1
        return $0
    }(UIPickerView(), yearPickerDataSource)
    
    private lazy var monthPickerDataSource: MonthPickerDataSourceAndDelegate = {
        return $0
    }(MonthPickerDataSourceAndDelegate())
    
    private lazy var monthPickerView: UIPickerView = {
        $0.showsSelectionIndicator = true
        $0.dataSource = $1
        $0.delegate = $1
      return $0
    }(UIPickerView(), monthPickerDataSource)
    
    @IBOutlet weak var segmentedControl: SegmentedControl!
    @IBOutlet weak var creditCardName: UITextField!
    @IBOutlet weak var creditCardExpireMonth: UITextField! {
        didSet {
            creditCardExpireMonth.inputView = monthPickerView
            creditCardExpireMonth.delegate = self
            monthPickerDataSource.didSelectMonth = { [weak self] monthInt, monthUI, monthName in
                self?.creditCardExpireMonth.text = monthUI
            }
            addToolBarInTextField(creditCardExpireMonth)
        }
    }
    @IBOutlet weak var creditCardExpireYear: UITextField! {
        didSet {
            creditCardExpireYear.inputView = yearPickerView
            creditCardExpireYear.delegate = self
            yearPickerDataSource.didSelectYear = { [weak self] yearUI in
                self?.creditCardExpireYear.text = yearUI.description
            }
            addToolBarInTextField(creditCardExpireYear)
        }
    }
    @IBOutlet weak var creditCardNumber: UITextField! {
        didSet {
            creditCardNumber.delegate = self
        }
    }
    @IBOutlet weak var creditCardCCV: UITextField! {
        didSet {
            creditCardCCV.delegate = self
        }
    }
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBAction func segmentedValueDidChange(_ sender: SegmentedControl) {
    }
    @IBAction func acceptWasPressed(_ sender: Any) {
    }
    
    @IBAction func backWasPressed(_ sender: Any) {
    }
}

extension PaymentsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.items = ["PayPal", "Tarjeta"]
        segmentedControl.selectedSegmentedIndex = 1
        navigationItem.title = NSLocalizedString("Pago", comment: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let items = ["At here you can share what you want"]
        ShareManager.shared.share(in: self, title: "Title", image: #imageLiteral(resourceName: "icon"), items: items) { item in
            print(item)
        }
    }
}

extension PaymentsViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == creditCardExpireMonth {
            textField.text = monthPickerDataSource.selectedItem.uiFormatted
        }
        
        if textField == creditCardExpireYear {
            textField.text = yearPickerDataSource.selectedYear.description
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == creditCardNumber {
            guard string.compactMap({ Int(String($0)) }).count == string.count else {
                return false
            }
            
            let text = textField.text ?? ""
            
            if string.count == 0 {
                textField.text = String(text.dropLast()).chunkFormatted(withChunkSize: 4, withSeparator: " ")
            } else {
                let formatted = String(text + string)
                    .filter({ $0 != " " })
                    .prefix(maxNumberOfCharacters)
                textField.text = String(formatted).chunkFormatted(withChunkSize: 4, withSeparator: " ")
            }
            
            return false
        }
        
        
        if textField == creditCardCCV {
            guard string.compactMap({ Int(String($0)) }).count == string.count else {
                return false
            }
            
            let text = textField.text ?? ""
            
            if string.count == 0 {
                textField.text = String(text.dropLast()).chunkFormatted(withChunkSize: 3, withSeparator: " ")
            } else {
                let formatted = String(text + string)
                    .filter({ $0 != " " })
                    .prefix(maxCVV)
                textField.text = String(formatted).chunkFormatted(withChunkSize: 3, withSeparator: " ")
            }
            
            return false
        }
        
        return true
        
    }
}

final class YearPickerDataSourceAndDelegate: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    private let years: [Int] = (2018...(2018 + 10)).map({ $0 })
    
    var didSelectYear: ( (Int) -> Void )?
    var selectedYear: Int = 2018
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return years.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return years[row].description
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedYear = years[row]
        didSelectYear?(years[row])
    }
    
}

final class MonthPickerDataSourceAndDelegate: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    private static let dateFormatter: DateFormatter = {
        $0.dateFormat = "MMMM"
        return $0
    }(DateFormatter())
    private let months: [(Int, String)] = (1...12).compactMap({
        var components = DateComponents()
        components.year = 2018
        components.month = $0
        components.day = 1
        guard let date = NSCalendar(identifier: NSCalendar.Identifier.gregorian)?.date(from: components) else {
            return nil
        }
        return ($0, MonthPickerDataSourceAndDelegate.dateFormatter.string(from: date))
    })
    
    var didSelectMonth: ( (Int, String,  String) -> Void )?
    lazy var selectedItem: (index: Int, uiFormatted: String,  monthName: String) = {
        return ($0[0].0, String(format: "%02d", $0[0].0), $0[0].1)
    }(months)
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return months.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return months[row].1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedItem = (months[row].0, String(format: "%02d", months[row].0), months[row].1)
        didSelectMonth?(months[row].0, String(format: "%02d", months[row].0), months[row].1)
    }
    
}

extension Collection {
    public func chunk(n: Int) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}

extension String {
    func chunkFormatted(withChunkSize chunkSize: Int = 4,
                        withSeparator separator: Character = "-") -> String {
        
        return
            self
            .filter { $0 != separator }
            .chunk(n: chunkSize)
            .map{ String($0) }
            .joined(separator: String(separator))
    }
}


