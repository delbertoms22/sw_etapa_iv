//
//  UpcomingEventDetailViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/26/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class UpcomingEventDetailViewController: UIViewController {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var eventNameLabel: UILabel!
    @IBOutlet private weak var eventDateLabel: UILabel!
    @IBOutlet private weak var buyButton: UIButton!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    
    @IBAction func buyWasPressed(_ sender: UIButton) {
        
    }
    
    @objc @IBAction private func backButtonWasPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension UpcomingEventDetailViewController {
    public override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}

extension UpcomingEventDetailViewController {
    
}

extension UpcomingEventDetailViewController {
    static func makeUpcomingEventDetailViewController() -> UpcomingEventDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: UpcomingEventDetailViewController.reuseId) as! UpcomingEventDetailViewController
    }
}
