//
//  ImagePageViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/26/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class ImagePageViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
}


extension ImagePageViewController {
    static func makeImagePageViewController() -> ImagePageViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: ImagePageViewController.reuseId)
        viewController.loadViewIfNeeded()
        return viewController as! ImagePageViewController
    }
}




