//
//  RoutinesViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/27/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class RoutinesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
            view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            tableView.tableFooterView = view
            tableView.estimatedRowHeight = 160
        }
    }
    var storedOffsets = [Int: CGFloat]()
}

extension RoutinesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
    }
}

extension RoutinesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell: LargeRoutineCell = tableView.dequeueReusableCell(at: indexPath)
            cell.didSelectCellAtIndexPath = { [weak self] ip in
                self?.performSegue(withIdentifier: RoutineDetailViewController.reuseId, sender: self)
            }
            return cell
        } else {
            let cell: StandardRoutineCell = tableView.dequeueReusableCell(at: indexPath)
            cell.didSelectCellAtIndexPath = { [weak self] ip in
                self?.performSegue(withIdentifier: RoutineDetailViewController.reuseId, sender: self)
            }
            return cell
        }
    }
    
    
    
}

extension RoutinesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
}


final class StandardRoutineCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            let nib = UINib(nibName: RoutineTitleImageCell.reuseId, bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: RoutineTitleImageCell.reuseId)
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    var didSelectCellAtIndexPath: ( (IndexPath) -> Void )?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RoutineTitleImageCell = collectionView.dequeueReusableCell(at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = collectionView.frame.size
        return CGSize(width: ((size.width * 0.85) / 1.95), height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCellAtIndexPath?(indexPath)
    }
}

final class LargeRoutineCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!  {
        didSet {
            let nib = UINib(nibName: RoutineImageCell.reuseId, bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: RoutineImageCell.reuseId)
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    var didSelectCellAtIndexPath: ( (IndexPath) -> Void )?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RoutineImageCell = collectionView.dequeueReusableCell(at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = collectionView.frame.size        
        return CGSize(width: size.width * 0.85, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCellAtIndexPath?(indexPath)
    }
}


final class RoutineImageCell: UICollectionViewCell {
    @IBOutlet weak var pictureView: UIImageView!
    
    
}

final class RoutineTitleImageCell: UICollectionViewCell {
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    
}
