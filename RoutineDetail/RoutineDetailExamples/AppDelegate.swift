//
//  AppDelegate.swift
//  RoutineDetailExamples
//
//  Created by Mario Canto on 7/25/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit
import RoutineDetail


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    typealias LaunchOptions = [UIApplicationLaunchOptionsKey: Any]
    
    lazy var window: UIWindow? = {
        return $0
    }(UIWindow(frame: UIScreen.main.bounds))
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: LaunchOptions?) -> Bool {
        
        let nav = UINavigationController(rootViewController: UIViewController())
        window?.rootViewController = nav
        nav.pushViewController(RoutineDetailViewController.makeRoutineDetailViewController(), animated: false)
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    
}

