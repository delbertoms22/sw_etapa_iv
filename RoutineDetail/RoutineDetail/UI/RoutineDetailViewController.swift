//
//  RoutineDetailViewController.swift
//  RoutineDetail
//
//  Created by Mario Canto on 7/25/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

public final class RoutineDetailViewController: UIViewController {
    
}

// MARK: UIViewController life cycle

extension RoutineDetailViewController {
    override public func loadView() {
        self.view = RoutineDetailView()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("Rutinas en casa", comment: "")
        let backButtonImage = UIImage(named: "btn_icon_back", in: Bundle.init(for: RoutineDetailView.self), compatibleWith: nil)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backButtonImage,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(self.backButtonWasPressed(_:)))
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.isTranslucent = true
        let attrs: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.medium)
        ]
        navigationController?.navigationBar.titleTextAttributes = attrs
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension RoutineDetailViewController {
    
    @objc private func backButtonWasPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: RoutinesViewController - Factory Methods

extension RoutineDetailViewController {
    public static func makeRoutineDetailViewController() -> RoutineDetailViewController {
        let viewController = RoutineDetailViewController(nibName: nil, bundle: nil)
        return viewController
    }
}
