//
//  RoutineDetailView.swift
//  RoutineDetail
//
//  Created by Mario Canto on 7/25/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import Commons


final class RoutineDetailView: UIView {
    
    private lazy var routineTitleLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.medium)
        $0.text = NSLocalizedString("Rutinas en casa", comment: "")
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var imageView: UIImageView = {
        $0.contentMode = .scaleAspectFill
        $0.image = UIImage(named: "girl", in: Bundle(for: RoutineDetailView.self), compatibleWith: nil)
        $0.clipsToBounds = true
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIImageView())
    
    
    private lazy var routineNameLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.medium)
        $0.text = "Body Pump"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var reproductionsCountLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        $0.text = "3,292"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var reproductionsLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        $0.text = NSLocalizedString("Reproducciones", comment: "")
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var likeButton: UIButton = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setImage(UIImage(named: "btn_like_normal", in: Bundle.init(for: RoutineDetailView.self), compatibleWith: nil), for: .normal)
        $0.setImage(UIImage(named: "btn_like_selected", in: Bundle.init(for: RoutineDetailView.self), compatibleWith: nil), for: .selected)
        $0.addTarget(self, action: #selector(self.heartWasPressed(_:)), for: .touchUpInside)
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIButton(type: .custom))
    
    
    private lazy var playButton: UIButton = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.1254901961, blue: 0.1647058824, alpha: 1)
        $0.setTitle(NSLocalizedString("REPRODUCIR", comment: ""), for: .normal)
        $0.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        
        return $0
    }(Button(type: .custom))
    
    
    private lazy var titleStackView: UIStackView = {
        $0.spacing = 8
        $0.axis = .horizontal
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [routineNameLabel, likeButton]))
    
    private lazy var reproductionsStackView: UIStackView = {
        $0.spacing = 8
        $0.axis = .horizontal
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [reproductionsCountLabel, reproductionsLabel]))
    
    
    private lazy var detailsStackView: UIStackView = {
        $0.spacing = 4
        $0.axis = .vertical
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [titleStackView, reproductionsStackView]))
    
    
    private func makeButton(_ tag: Int) -> UIButton {
        let btn = UIButton(type: .custom)
        btn.setContentCompressionResistancePriority(.required, for: .horizontal)
        btn.setContentCompressionResistancePriority(.required, for: .vertical)
        btn.setContentHuggingPriority(.required, for: .horizontal)
        btn.setContentHuggingPriority(.required, for: .vertical)
        btn.setImage(UIImage(named: "btn_star_normal", in: Bundle.init(for: RoutineDetailView.self), compatibleWith: nil), for: .normal)
        btn.setImage(UIImage(named: "btn_star_selected", in: Bundle.init(for: RoutineDetailView.self), compatibleWith: nil), for: .selected)
        btn.addTarget(self, action: #selector(self.starWasPressed(_:)), for: .touchUpInside)
        btn.tag = tag
        return btn
    }
    private lazy var rankingButtons: [UIButton] = {
        return (0...4).enumerated().map({ makeButton($0.0) })
    }()
    
    private lazy var starsStackView: UIStackView = {
        $0.spacing = 8
        $0.axis = .horizontal
        $0.distribution = .fillEqually
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: rankingButtons))
    
    private lazy var gradientView: UIView = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.startColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        $0.endColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        $0.startPoint = CGPoint(x: 0.5, y: 0.0)
        $0.endPoint = CGPoint(x: 0.5, y: 0.5)
        return $0
    }(GradientView(frame: UIScreen.main.bounds))
    
    
    private lazy var routineDescriptionLabel: UILabel = {
        $0.textAlignment = .justified
        $0.lineBreakMode = .byWordWrapping
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
        $0.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin convallis eleifend sagittis. "
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.numberOfLines = 0
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        return $0
    }(UILabel())
    
    private lazy var trainerLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        $0.text = NSLocalizedString("Entrenador", comment: "")
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        return $0
    }(UILabel())
    
    private lazy var trainerImageView: UIImageView = {
        $0.layer.cornerRadius = 6
        $0.contentMode = .scaleAspectFill
        $0.image = UIImage(named: "RUTINAS_NET_DETALLE 2", in: Bundle(for: RoutineDetailView.self), compatibleWith: nil)
        $0.clipsToBounds = true
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIImageView())
    
    
    private lazy var trainerNameLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.numberOfLines = 2
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = NSLocalizedString("Ana Rojas", comment: "")
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    
    
    private lazy var starImageViews: [UIButton] = {
        let items = (0...4).enumerated().map({ makeButton($0.0) })
        
        items.forEach({
            $0.imageView?.contentMode = .scaleAspectFit
            $0.isUserInteractionEnabled = false
        })
        return items
    }()
    
    
    
    private lazy var starsImagesStackView: UIStackView = {
        // TODO: remove $0.isHidden = true, this is hidding the ranking control for trainer
        $0.isHidden = true
        $0.spacing = 0
        $0.axis = .horizontal
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: starImageViews))
    
    
    private lazy var trainerDescriptionLabel: UILabel = {
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin convallis eleifend sagittis. "
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.numberOfLines = 0
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        return $0
    }(UILabel())
    
    convenience init() {
        self.init(frame: UIScreen.main.bounds)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews() {
        backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        addSubview(imageView)
        addSubview(gradientView)
        addSubview(titleStackView)
        addSubview(reproductionsStackView)
        addSubview(starsStackView)
        addSubview(playButton)
        addSubview(routineDescriptionLabel)
        addSubview(trainerLabel)
        addSubview(trainerImageView)
        addSubview(trainerNameLabel)
        addSubview(starsImagesStackView)
        addSubview(trainerDescriptionLabel)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            imageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5),
            titleStackView.topAnchor.constraint(equalTo: topAnchor, constant: bounds.height * 0.35),
            titleStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            reproductionsStackView.topAnchor.constraint(equalTo: titleStackView.bottomAnchor, constant: 4),
            reproductionsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            starsStackView.topAnchor.constraint(equalTo: reproductionsStackView.bottomAnchor, constant: 8),
            starsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            playButton.topAnchor.constraint(equalTo: starsStackView.bottomAnchor, constant: 16),
            playButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            playButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32),
            routineDescriptionLabel.topAnchor.constraint(equalTo: playButton.bottomAnchor, constant: 16),
            routineDescriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            routineDescriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32),
            trainerLabel.topAnchor.constraint(equalTo: routineDescriptionLabel.bottomAnchor, constant: 16),
            trainerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            trainerImageView.topAnchor.constraint(equalTo: trainerLabel.bottomAnchor, constant: 16),
            trainerImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            trainerImageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.2),
            trainerImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            trainerNameLabel.topAnchor.constraint(equalTo: trainerLabel.bottomAnchor, constant: 16),
            trainerNameLabel.leadingAnchor.constraint(equalTo: trainerImageView.trailingAnchor, constant: 16),
            starsImagesStackView.centerYAnchor.constraint(equalTo: trainerNameLabel.centerYAnchor),
            starsImagesStackView.leadingAnchor.constraint(equalTo: trainerNameLabel.trailingAnchor, constant: 16),
            starsImagesStackView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -16),
            starsImagesStackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.25),
            starsImagesStackView.heightAnchor.constraint(equalToConstant: 10),
            trainerDescriptionLabel.topAnchor.constraint(equalTo: starsImagesStackView.bottomAnchor, constant: 16),
            trainerDescriptionLabel.leadingAnchor.constraint(greaterThanOrEqualTo: trainerImageView.trailingAnchor, constant: 16),
            trainerDescriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            trainerDescriptionLabel.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -16),
            gradientView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            gradientView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            gradientView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            gradientView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
            ])
    }
}

extension RoutineDetailView {
    
    @objc private func heartWasPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @objc private func starWasPressed(_ sender: UIButton) {
        rankingButtons
            .forEach({ $0.isSelected = false })
        rankingButtons
            .filter({ $0.tag <= sender.tag })
            .forEach({ $0.isSelected = true })
        
        starImageViews
            .forEach({ $0.isSelected = false })
        starImageViews
            .filter({ $0.tag <= sender.tag })
            .forEach({ $0.isSelected = true })
        
    }
}
