//
//  EventsViewController.swift
//  EventsList
//
//  Created by Mario Canto on 7/25/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

public final class EventsViewController: UIViewController {
    
}

extension EventsViewController {
    override public func loadView() {
        self.view = EventsView()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
    }
}

// MARK: EventsViewController - Factory Methods

extension EventsViewController {
    public static func makeEventsViewController() -> EventsViewController {
        let viewController = EventsViewController(nibName: nil, bundle: nil)
        return viewController
    }
}
