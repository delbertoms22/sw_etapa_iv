//
//  EventsView.swift
//  EventsList
//
//  Created by Mario Canto on 7/25/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import Commons

final class SegmentedControlContainerView: UIControl {
    
    var selectedSegmentedIndex: Int {
        set {
            segmentedControl.selectedSegmentIndex = newValue
        }
        get {
            return segmentedControl.selectedSegmentIndex
        }
    }
    
    lazy var segmentedControl: UISegmentedControl = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        $0.addTarget($1, action: #selector($1.segmentedControlDidChangeValue(_:)), for: .valueChanged)
        $0.selectedSegmentIndex = 0
        return $0
    }(UISegmentedControl.init(items: [NSLocalizedString("Próximos", comment: ""), NSLocalizedString("Pasados", comment: "")]), self)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(segmentedControl)
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            segmentedControl.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            segmentedControl.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            segmentedControl.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            ])
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        layer.borderColor = tintColor.cgColor
        layer.borderWidth = 1
    }
    
    @objc private func segmentedControlDidChangeValue(_ sender: UISegmentedControl) {
        sendActions(for: .valueChanged)
    }
}

final class PastEventContainerView: UIView {
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16 * 2, height: UIScreen.main.bounds.height / 3.0)
    }
}

final class UpcomingEventContainerView: UIView {
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16 * 2, height: UIScreen.main.bounds.height / 9.0)
    }
}

final class EventDateContainerView: UIView {
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width * 0.25, height: UIScreen.main.bounds.height / 9.0)
    }
}

final class EventsView: UIView {

    private lazy var collectionViewFlowLayout: UICollectionViewFlowLayout = {
        $0.scrollDirection = .vertical
        $0.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width, height: 64)
        $0.sectionInset = UIEdgeInsets(top: 16 + 16 + 34, left: 0, bottom: 16, right: 0)
        return $0
    }(UICollectionViewFlowLayout())
    
    private lazy var collectionView: UICollectionView = {
        $0.register(UpcomingEventsCollectionViewCell.self, forCellWithReuseIdentifier: UpcomingEventsCollectionViewCell.reuseId)
        $0.register(PastEventsCollectionViewCell.self, forCellWithReuseIdentifier: PastEventsCollectionViewCell.reuseId)
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.dataSource = $1
        return $0
    }(UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewFlowLayout), self)

    private lazy var segmentedControl: UISegmentedControl = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UISegmentedControl.init(items: [NSLocalizedString("Próximos", comment: ""), NSLocalizedString("Pasados", comment: "")]))
    
    private lazy var segmentedControlContainerView: UIControl = {
        $0.tintColor = #colorLiteral(red: 0.9176470588, green: 0.1098039216, blue: 0.1333333333, alpha: 1)
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(SegmentedControlContainerView(frame: CGRect.zero))
    
    convenience init() {
        self.init(frame: UIScreen.main.bounds)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    enum CellType: Int {
        case upcoming
        case past
    }
    
    private var cellType: CellType = .upcoming
    
    @objc
    func pressed(_ sender: SegmentedControlContainerView) {
        print(sender.selectedSegmentedIndex)
        cellType = CellType(rawValue: sender.selectedSegmentedIndex)!
        collectionView.reloadData()
    }
    
    private func setupSubviews() {
        
        addSubview(collectionView)
        addSubview(segmentedControlContainerView)
        
        segmentedControlContainerView.addTarget(self, action: #selector(self.pressed(_:)), for: .valueChanged)
        
        NSLayoutConstraint.activate([
            segmentedControlContainerView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            segmentedControlContainerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            segmentedControlContainerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32),
            segmentedControlContainerView.heightAnchor.constraint(equalToConstant: 34),
            collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
            ])
    }
}

extension EventsView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch cellType {
        case .upcoming:
          let cell: UpcomingEventsCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
            return cell
        case .past:
            let cell: PastEventsCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
            return cell
        }
//        let cell: UpcomingEventsCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
        
        
    }
}

// MARK:- PastEventsCollectionViewCell

final class PastEventsCollectionViewCell: UICollectionViewCell {
    
    private lazy var titleLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = "InBike"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var locationLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = "World Trade Center"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var eventStackView: UIStackView = {
        $0.spacing = 0
        $0.axis = .vertical
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [titleLabel, locationLabel]))
    
    private lazy var containerView: UIView = {
        $0.layer.cornerRadius = 3
        $0.layer.masksToBounds = true
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(PastEventContainerView(frame: CGRect.zero))
    
    private lazy var bottomContainerView: UIView = {
        $0.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIView(frame: CGRect.zero))
    
    private lazy var imageView: UIImageView = {
        $0.contentMode = .scaleAspectFill
        $0.image = UIImage(named: "dog", in: Bundle(for: PastEventsCollectionViewCell.self), compatibleWith: nil)
        $0.clipsToBounds = true
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIImageView())
    
    private func makeImageView() -> UIImageView {
        let image = UIImage(named: "btn_star_selected", in: Bundle(for: PastEventsCollectionViewCell.self), compatibleWith: nil)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private lazy var starImageViews: [UIImageView] = {
        let items = (0...4).map({ _ in makeImageView() })
        return items
    }()
    
    private lazy var starsImagesStackView: UIStackView = {
        $0.isHidden = false
        $0.spacing = 4
        $0.axis = .horizontal
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: starImageViews))
    
    private let raitingsSectionFont = UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.regular)
    
    private lazy var ratingLeadingLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        $0.font = $1
        $0.text = "("
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel(), raitingsSectionFont)
    
    private lazy var ratingTrailingLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        $0.font = $1
        $0.text = ")"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel(), raitingsSectionFont)
    
    private lazy var ratingsLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        $0.font = $1
        $0.text = NSLocalizedString("calificaciones", comment: "")
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel(), raitingsSectionFont)
    
    private lazy var ratingsCountLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        $0.font = $1
        $0.text = "12"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel(), raitingsSectionFont)
    
    private lazy var raitingsStackView: UIStackView = {
        $0.isHidden = false
        $0.spacing = 4
        $0.axis = .horizontal
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [ratingLeadingLabel,ratingsCountLabel, ratingsLabel, ratingTrailingLabel]))
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews() {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        contentView.addSubview(containerView)
        

        containerView.addSubview(imageView)
        containerView.addSubview(bottomContainerView)
        bottomContainerView.addSubview(eventStackView)
        bottomContainerView.addSubview(starsImagesStackView)
        bottomContainerView.addSubview(raitingsStackView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            containerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 0),
            imageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            imageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            bottomContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            bottomContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            bottomContainerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            bottomContainerView.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.5),
            eventStackView.bottomAnchor.constraint(equalTo: bottomContainerView.centerYAnchor, constant: 0),
            eventStackView.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor, constant: 32),
            starsImagesStackView.topAnchor.constraint(equalTo: bottomContainerView.centerYAnchor, constant: 16),
            starsImagesStackView.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor, constant: 32),
            starsImagesStackView.heightAnchor.constraint(equalToConstant: 14),
            starsImagesStackView.widthAnchor.constraint(equalTo: bottomContainerView.widthAnchor, multiplier: 0.2),
            raitingsStackView.leadingAnchor.constraint(equalTo: starsImagesStackView.trailingAnchor, constant: 16),
            ratingsLabel.centerYAnchor.constraint(equalTo: starsImagesStackView.centerYAnchor, constant: 0)
            ])
    }
}

final class UpcomingEventsCollectionViewCell: UICollectionViewCell {
    
    private lazy var dateLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.medium)
        $0.text = "24"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var dayLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = "Hoy"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var dateStackView: UIStackView = {
        $0.spacing = 0
        $0.axis = .vertical
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [dateLabel, dayLabel]))
    
    private lazy var dateContainerView: UIView = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(EventDateContainerView(frame: CGRect.zero))
    
    
    private lazy var titleLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = "InBike"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var locationLabel: UILabel = {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        $0.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        $0.text = "World Trade Center"
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UILabel())
    
    private lazy var eventStackView: UIStackView = {
        $0.spacing = 0
        $0.axis = .vertical
        $0.distribution = .fillProportionally
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIStackView(arrangedSubviews: [titleLabel, locationLabel]))
    
    private lazy var containerView: UIView = {
        $0.layer.cornerRadius = 6
        $0.layer.masksToBounds = true
        $0.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).cgColor
        $0.layer.borderWidth = 1
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .vertical)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .vertical)
        $0.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UpcomingEventContainerView(frame: CGRect.zero))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews() {

        addSubview(containerView)
        containerView.addSubview(dateContainerView)
        dateContainerView.addSubview(dateStackView)
        containerView.addSubview(eventStackView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            containerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 0),
            containerView.heightAnchor.constraint(equalToConstant: 64),
            dateContainerView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            dateContainerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            dateContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            dateStackView.centerXAnchor.constraint(equalTo: dateContainerView.centerXAnchor, constant: 0),
            dateStackView.centerYAnchor.constraint(equalTo: dateContainerView.centerYAnchor, constant: 0),
            eventStackView.leadingAnchor.constraint(equalTo: dateContainerView.trailingAnchor, constant: 24),
            eventStackView.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -16),
            eventStackView.centerYAnchor.constraint(equalTo: dateContainerView.centerYAnchor, constant: 0)
            ])
    }
}
